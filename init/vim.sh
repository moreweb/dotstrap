# environment specific variables
BUNDLE_DIR="${HOME}/.vim/bundle"

# package list
centos_packages() {
    packages="vim"
}
ubuntu_packages() {
    packages="vim"
}

${DISTRO_NAME_L}_packages
# run package installer
${INSTALLER} ${packages}

rm -rf ${BUNDLE_DIR}
mkdir -p ${BUNDLE_DIR}

# Install Vundle if no directory.
if [ ! -d $HOME/.vim/bundle/vundle ]; then
  git clone https://github.com/gmarik/vundle.git $HOME/.vim/bundle/vundle
fi

# kill our .vimrc
[ -f ~/.vimrc ] && rm -f ~/.vimrc

# rebuild .vimrc
cat > ~/.vimrc <<__EOF__
"""
" Vundle Settings "
"""
set nocompatible
filetype off

set rtp+=~/.vim/bundle/vundle/
call vundle#begin()

Bundle "gmarik/vundle"
Bundle "mileszs/ack.vim"
Bundle "kien/ctrlp.vim"
Bundle "flazz/vim-colorschemes"
Bundle "bling/vim-airline"
Bundle "airblade/vim-gitgutter"
Bundle "nanotech/jellybeans.vim"
Bundle "vim-ruby/vim-ruby"
Bundle "pangloss/vim-javascript"
Bundle "plasticboy/vim-markdown"
Bundle "vim-scripts/sudo.vim"

call vundle#end()

syntax on
filetype plugin indent on
""""

color jellybeans

set ts=4
set tabstop=4
set expandtab
set softtabstop=4
set shiftwidth=4
set number
set smartindent
set title
set whichwrap+=<>[]hl
set laststatus=2
set cursorline
set background=dark
set incsearch
set hlsearch
set nowritebackup
set noswapfile
set nobackup
__EOF__

# install all VIM plugins
echowarn "  call 'vim +BundleInstall +qa' to install all bundle"
