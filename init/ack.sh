# package list
centos_packages() {
    packages="ack"
}
ubuntu_packages() {
    packages="ack"
}

#TODO
#fix problem with repo
#su -c 'rpm -Uvh http://download.fedoraproject.org/pub/epel/6/i386/epel-release-6-8.noarch.rpm'
#su -c 'rpm -Uvh http://download.fedoraproject.org/pub/epel/7/x86_64/e/epel-release-7-2.noarch.rpm'
#yum install ack

${DISTRO_NAME_L}_packages
# run package installer
${INSTALLER} ${packages}

# kill our .ackrc
[ -f ~/.ackrc ] && rm -f ~/.ackrc

# rebuild .ackrc
cat > ~/.ackrc <<__EOF__
--type-add
css=.sass,.scss
__EOF__
