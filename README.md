dotstrap
========

CentOs and Ubuntu system configuration bootstrapping (vim, tmux, bash, ack, etc)

HowTo
-----

```
curl -L https://bitbucket.org/moreweb/dotstrap/raw/06dfa886ad085c93262abb57db31945cb455f48f/bootstrap.sh | sh -s
```
